# Movie Recommendation System Simulation

## Synopsis

The simulation of recommendation system for recommending movies to users, and predicting ratings users could give to movies they have not rated yet.

## Motivation

In this project, we will analyse the data of movies, users and votes taken from this [link](http://www.vukmalbasa.com/poslovna-inteligencija/file/movie%20recommend%20data.zip?attredirects=0&d=1).
to accomplish the movie rating prediction task. The two prevalent approaches to build recommendation systems are Collaborative Filtering and Content-Based recommending approaches. 
Collaborative filtering (social filtering) methods are based on exploiting similarities among profiles of users. 
Content-based methods are determined by features of the movies and the movies meeting the interests of given user.
We will train models with Item-Based and User-Based K nearest neighbours. These models belong to Collaborative filtering methods.
We will discuss positive and negative sides of our implementation. 
Lastly, we will calculate the rating prediction errors of each model in oreder to see if the output is close to the correct value.

## Project Structure
Order of scripts:
1. [Analyze Dataset](http://nbviewer.jupyter.org/urls/gitlab.com/jelena-b94/Movie_Recommendation_System/raw/master/notebooks/Analyzing%20Data-set.ipynb)
2. [Movie Similarity Matrix Exploration](http://nbviewer.jupyter.org/urls/gitlab.com/jelena-b94/Movie_Recommendation_System/raw/master/notebooks/Movies%20Similarity%20Matrix%20Exploration.ipynb)
3. [Item-Based Recommendation System](http://nbviewer.jupyter.org/urls/gitlab.com/jelena-b94/Movie_Recommendation_System/raw/master/notebooks/Item-Based%20Recommendation%20System.ipynb)
4. [User-Based Recommendation System](http://nbviewer.jupyter.org/urls/gitlab.com/jelena-b94/Movie_Recommendation_System/raw/master/notebooks/User-Based%20Recommendation%20System.ipynb)
5. [Degree Distribution](http://nbviewer.jupyter.org/urls/gitlab.com/jelena-b94/Movie_Recommendation_System/raw/master/notebooks/Degree%20Distribution.ipynb)

Directories:
- `data/*` - movie, user and vote datasets
- `htmls/*` - quick look with equations
- `notebooks/*` - detailed view with possibility of execution

## Installation
In case one is interested to also play with the output, `jupyter notebook` environment should be set up.

Required instalations:
- [Python 3.6.0](https://www.python.org/downloads/release/python-360/)
  
Choose between these two:
- [Anaconda 3](https://www.continuum.io/downloads)
- [Test Drive - Conda](https://conda.io/docs/test-drive.html) for 30-minutes test drive
  
or just `pip install jupyter` in order to run jupyter notebooks.

Later, `imdbpie` python package would be required. Installation is simple using: `pip install imdbpie`.

## API Reference

- [NumPy](http://www.numpy.org/)
- [Pandas](http://pandas.pydata.org/)
- [Matplotlib](https://matplotlib.org/)
- [Scikit-learn](http://scikit-learn.org/)
- [imdbpie](https://pypi.python.org/pypi/imdbpie)

## Author

**Jelena Banjac** Student of Software Engineering and Information Technologies, University of Novi Sad, Serbia

## Mentor

**Dr. Vuk Malbaša** Professor at Faculty of Technical Sciences, University of Novi Sad, Serbia