
# User-Based Recommendation System

User-Based K nearest neighbours model is rather similar to Item-Based KNN.  
The only difference is that we calculate similarity between each pair of users and predict the rating of user **u** on movie **m** by looking at the top K users that are similar to user **u**, and produce a prediction by calculating the wighted average of ratings from these users on the movie.

### Load dependencies


```python
# load dependencies
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
import requests
from imdbpie import Imdb
from sklearn.metrics import pairwise_distances,mean_squared_error,mean_absolute_error
from numpy import nan
```

### Load data


```python
data_dir = 'data/'

# users file
user_columns = ['user_id', 'age', 'sex', 'zip_code']
users = pd.read_csv(data_dir + 'Person.txt', sep='\t', names=user_columns, encoding='latin-1')

# ratings file
vote_columns = ['user_id', 'movie_id', 'rating1', 'rating2', 'unix_timestamp']
votes = pd.read_csv(data_dir + 'Vote.txt', sep='\t', names=vote_columns, encoding='latin-1')

# movies file
movie_columns = ['movie_id', 'movie_title', 'site_link', 'IMDb_URL', 'release_date', 'release_date_relative', 
                 'video_release_date', 'video_release_date_relative', 'unknown', 'Action', 'Adventure',
                 'Animation', 'Children\'s', 'Comedy', 'Crime', 'Documentary', 'Drama', 'Fantasy']
movies = pd.read_csv(data_dir + 'Movie.txt', sep='\t', names=movie_columns, encoding='latin-1')

# merge users, movies, and votes data
movie_ratings = pd.merge(movies, votes)
merged_table = pd.merge(movie_ratings, users)

```

Same as in Item-Based KNN, since our user's and movie's IDs are not continual, we will need to make dictionaries that will make a relation between user ID and user Index, and movie ID and movie Index in users and movies dictionaries, respectively.  
Then we will be able to easily map *user/movie ID's* to *user/item indices* by using these dictionaries.


```python
user_indices = {user.user_id: user.Index for user in users.itertuples()}
user_by_index = {user.Index: user for user in users.itertuples()}

movie_indices = {movie.movie_id: movie.Index for movie in movies.itertuples()}
movie_by_index = {movie.Index: movie for movie in movies.itertuples()}
```

### Prepare data and functions

Same as in Item-Based KNN, we will create matrix that consists of a user ratings of movies - **user-movie matrix**.  
The position of the *rating* is determined by *user Id* (as 0th axis) and *movie Id* (as 1st axis).


```python
def create_ratings_matrix():
    
    total_users = users.shape[0]
    total_movies = movies.shape[0]
    total_votes = votes.shape[0]
    
    ratings_matrix = np.zeros((total_users, total_movies))
    for row in votes.itertuples():
        ratings_matrix[user_indices[row.user_id], movie_indices[row.movie_id]] = row.rating1
    
    return ratings_matrix


ratings_matrix = create_ratings_matrix()
```

Also the same as in Item-Based KNN, we will split our data into training and test sets by removing 10 random ratings per user (`size=10`) from the training set and place them in the test set. This provides us with a possibility to check whether our recommendation system works appropriately.


```python
def train_test_split(ratings):
    test = np.zeros(ratings.shape)
    train = ratings.copy()
    for user in range(ratings.shape[0]):
        if ratings[user, :].nonzero()[0].size:
            test_ratings = np.random.choice(ratings[user, :].nonzero()[0], size=10, replace=True)
            train[user, test_ratings] = 0.
            test[user, test_ratings] = ratings[user, test_ratings]

    # Test and training are truly disjoint
    assert (np.all((train * test) == 0))
    return train, test

```

Similarity matrix will be created differently this time.  
The same as in Item-Based KNN, we will calculate similarity matrix using the built-in scikit-learn function. By choosing `metrc='correlation'`, we will be using the *Pearson correlation* to determine item similarity matrix.  
However, this time we will send our train matrix, not transposed train matrix. That means that output similarity matrix `item_correlation` will have size **72916 x 72916** instead of **1623 x 1623**. Therefore, I suppose we will have some problems with RAM memory, but let's see later...


```python
item_correlation = 1 - pairwise_distances(train.T, metric='correlation')
item_correlation[np.isnan(item_correlation)] = 0
```

Fuction that will return the list of users that are most similar to the input user.


```python
def top_k_users(similarity, mapper, user_idx, k):
    return [mapper[x] for x in np.argsort(similarity[:, user_idx])[:-k-1:-1]]
```

Function for calculating Mean Squared Error (MSE) and Mean Absolute Error (MAE).


```python
def calculate_error(real_ratings, predicted_ratings):
    MSE = mean_squared_error(real_ratings, predicted_ratings)
    MAE = mean_absolute_error(real_ratings, predicted_ratings)
    return MSE, MAE
```

We will use the same way of calculating the prediction rating of a movie as in Item-Based KNN.  
We use the following function to predict the value of ratings user **u** gives to movie **m** as a weighted average of similar movies user **m** has rated.

\begin{align}
{r}_{u,m} & = \frac{\sum_{n \in N_{u}^k (m)} similarity(m,n) * {r}_{u,n}}{\sum_{n \in N_{u}^k (m)} similarity(m,n)} \\
\end{align}


```python
def user_based_K_nearest_neighbours(user_index, movie_index, k):
    # PREDICT
    predicted_rating = 0
    for r,c in zip(similar_users_Ratings[1:], item_correlation[user_index, similar_users_Indices[1:]]):
        predicted_rating += r*c
    predicted_rating /= np.sum(item_correlation[user_index, similar_users_Indices[1:]])
    
    # REAL
    real_rating = similar_users_Ratings[0]
    
    #print(f'prediction: {predicted_rating}')
    return predicted_rating, real_rating

```

### User-Based KNN

Finally, the same as in Item-Based KNN, we are able to see how much our prediction ratings are similar to real ratings user gave to movies.  
These real ratings we collected inside a test matrix and substracted this information from train matrix. 
In each iteration through users and movies, we find top K movies that are the most similar to the given movie.
Afterwards, we predict the rating user gave to movie.
Lastly, we calculate MSE and MAE errors in order to see how eficiently we completed our task.


```python
predicted_rating_list = {}
true_rating_list = {}
error = {}
 
train, test = train_test_split(ratings_matrix)   
#print(train.shape)
#print(train.T.shape)
item_correlation = 1 - pairwise_distances(train, metric='correlation')
item_correlation[np.isnan(item_correlation)] = 0

for k in range(2,11): # from 2 because 1 cannot give rating to itself
    predicted_rating_list = []
    true_rating_list = []
    for user_index in range(test.shape[0]):
        if test[user_index, :].nonzero()[0].size:
            for movie_index in test[user_index, :].nonzero()[0]:
                #print(user_index)

                similar_users = top_k_users(item_correlation, user_by_index, user_index, k)
                similar_users_Indices = [user.Index for user in similar_users] 
                similar_users_Ratings = ratings_matrix[similar_users_Indices,movie_index]
                
                predicted_rating, true_rating = user_based_K_nearest_neighbours(user_index, movie_index, k)
                #print(f"predicted: {predicted_rating}, true: {true_rating}")
                predicted_rating_list.append(predicted_rating)
                true_rating_list.append(true_rating)
    
    MSE, MAE = calculate_error(true_rating_list, predicted_rating_list)
    error[k] = MSE, MAE
    print(f'MSE = {MSE}, MAE = {MAE}')
  
```


    ---------------------------------------------------------------------------

    MemoryError                               Traceback (most recent call last)

    <ipython-input-11-fda7adb3a2e0> in <module>()
         10 #item_correlation[np.isnan(item_correlation)] = 0
         11 #print(item_correlation.shape)
    ---> 12 item_correlation = fast_user_similarity(train)
         13 
         14 for k in range(2,11): # from 2 because 1 cannot give rating to itself
    

    <ipython-input-9-c3850575a709> in fast_user_similarity(ratings, epsilon)
          1 def fast_user_similarity(ratings, epsilon=1e-9):
    ----> 2     sim = ratings.dot(ratings.T) + epsilon
          3     norms = np.array([np.sqrt(np.diagonal(sim))])
          4     return ( sim / norms / norms.T)
    

    MemoryError: 


Yes, we can see that we surely have a problem with memory. The size of user's similarity matrix is 72916 x 72916 which is much bigger than what could my 4GB RAM handle.  
  
While exploring on the Internet, I have found that the best solution to this problem would be not to use similarity matrix. Besides the fact that similarity matrices are usualy very sparse,  this case when it gets very big is one of the additional reasons why not to use similarity matrices in similar situations.
