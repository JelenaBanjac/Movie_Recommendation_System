{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# User-Based Recommendation System"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "User-Based K nearest neighbours model is rather similar to Item-Based KNN.  \n",
    "The only difference is that we calculate similarity between each pair of users and predict the rating of user **u** on movie **m** by looking at the top K users that are similar to user **u**, and produce a prediction by calculating the wighted average of ratings from these users on the movie."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Load dependencies"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# load dependencies\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "import sys\n",
    "import requests\n",
    "from imdbpie import Imdb\n",
    "from sklearn.metrics import pairwise_distances,mean_squared_error,mean_absolute_error\n",
    "from numpy import nan"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Load data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "data_dir = '../data/'\n",
    "\n",
    "# users file\n",
    "user_columns = ['user_id', 'age', 'sex', 'zip_code']\n",
    "users = pd.read_csv(data_dir + 'Person.txt', sep='\\t', names=user_columns, encoding='latin-1')\n",
    "\n",
    "# ratings file\n",
    "vote_columns = ['user_id', 'movie_id', 'rating1', 'rating2', 'unix_timestamp']\n",
    "votes = pd.read_csv(data_dir + 'Vote.txt', sep='\\t', names=vote_columns, encoding='latin-1')\n",
    "\n",
    "# movies file\n",
    "movie_columns = ['movie_id', 'movie_title', 'site_link', 'IMDb_URL', 'release_date', 'release_date_relative', \n",
    "                 'video_release_date', 'video_release_date_relative', 'unknown', 'Action', 'Adventure',\n",
    "                 'Animation', 'Children\\'s', 'Comedy', 'Crime', 'Documentary', 'Drama', 'Fantasy']\n",
    "movies = pd.read_csv(data_dir + 'Movie.txt', sep='\\t', names=movie_columns, encoding='latin-1')\n",
    "\n",
    "# merge users, movies, and votes data\n",
    "movie_ratings = pd.merge(movies, votes)\n",
    "merged_table = pd.merge(movie_ratings, users)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Same as in Item-Based KNN, since our user's and movie's IDs are not continual, we will need to make dictionaries that will make a relation between user ID and user Index, and movie ID and movie Index in users and movies dictionaries, respectively.  \n",
    "Then we will be able to easily map *user/movie ID's* to *user/item indices* by using these dictionaries."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "user_indices = {user.user_id: user.Index for user in users.itertuples()}\n",
    "user_by_index = {user.Index: user for user in users.itertuples()}\n",
    "\n",
    "movie_indices = {movie.movie_id: movie.Index for movie in movies.itertuples()}\n",
    "movie_by_index = {movie.Index: movie for movie in movies.itertuples()}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Prepare data and functions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Same as in Item-Based KNN, we will create matrix that consists of a user ratings of movies - **user-movie matrix**.  \n",
    "The position of the *rating* is determined by *user Id* (as 0th axis) and *movie Id* (as 1st axis)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def create_ratings_matrix():\n",
    "    \n",
    "    total_users = users.shape[0]\n",
    "    total_movies = movies.shape[0]\n",
    "    total_votes = votes.shape[0]\n",
    "    \n",
    "    ratings_matrix = np.zeros((total_users, total_movies))\n",
    "    for row in votes.itertuples():\n",
    "        ratings_matrix[user_indices[row.user_id], movie_indices[row.movie_id]] = row.rating1\n",
    "    \n",
    "    return ratings_matrix\n",
    "\n",
    "\n",
    "ratings_matrix = create_ratings_matrix()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Also the same as in Item-Based KNN, we will split our data into training and test sets by removing 10 random ratings per user (`size=10`) from the training set and place them in the test set. This provides us with a possibility to check whether our recommendation system works appropriately."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def train_test_split(ratings):\n",
    "    test = np.zeros(ratings.shape)\n",
    "    train = ratings.copy()\n",
    "    for user in range(ratings.shape[0]):\n",
    "        if ratings[user, :].nonzero()[0].size:\n",
    "            test_ratings = np.random.choice(ratings[user, :].nonzero()[0], size=10, replace=True)\n",
    "            train[user, test_ratings] = 0.\n",
    "            test[user, test_ratings] = ratings[user, test_ratings]\n",
    "\n",
    "    # Test and training are truly disjoint\n",
    "    assert (np.all((train * test) == 0))\n",
    "    return train, test\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similarity matrix will be created differently this time.  \n",
    "The same as in Item-Based KNN, we will calculate similarity matrix using the built-in scikit-learn function. By choosing `metrc='correlation'`, we will be using the *Pearson correlation* to determine item similarity matrix.  \n",
    "However, this time we will send our train matrix, not transposed train matrix. That means that output similarity matrix `item_correlation` will have size **72916 x 72916** instead of **1623 x 1623**. Therefore, I suppose we will have some problems with RAM memory, but let's see later..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "item_correlation = 1 - pairwise_distances(train.T, metric='correlation')\n",
    "item_correlation[np.isnan(item_correlation)] = 0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Fuction that will return the list of users that are most similar to the input user."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def top_k_users(similarity, mapper, user_idx, k):\n",
    "    return [mapper[x] for x in np.argsort(similarity[:, user_idx])[:-k-1:-1]]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "Function for calculating Mean Squared Error (MSE) and Mean Absolute Error (MAE)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def calculate_error(real_ratings, predicted_ratings):\n",
    "    MSE = mean_squared_error(real_ratings, predicted_ratings)\n",
    "    MAE = mean_absolute_error(real_ratings, predicted_ratings)\n",
    "    return MSE, MAE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will use the same way of calculating the prediction rating of a movie as in Item-Based KNN.  \n",
    "We use the following function to predict the value of ratings user **u** gives to movie **m** as a weighted average of similar movies user **m** has rated."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\\begin{align}\n",
    "{r}_{u,m} & = \\frac{\\sum_{n \\in N_{u}^k (m)} similarity(m,n) * {r}_{u,n}}{\\sum_{n \\in N_{u}^k (m)} similarity(m,n)} \\\\\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def user_based_K_nearest_neighbours(user_index, movie_index, k):\n",
    "    # PREDICT\n",
    "    predicted_rating = 0\n",
    "    for r,c in zip(similar_users_Ratings[1:], item_correlation[user_index, similar_users_Indices[1:]]):\n",
    "        predicted_rating += r*c\n",
    "    predicted_rating /= np.sum(item_correlation[user_index, similar_users_Indices[1:]])\n",
    "    \n",
    "    # REAL\n",
    "    real_rating = similar_users_Ratings[0]\n",
    "    \n",
    "    #print(f'prediction: {predicted_rating}')\n",
    "    return predicted_rating, real_rating\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### User-Based KNN"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, the same as in Item-Based KNN, we are able to see how much our prediction ratings are similar to real ratings user gave to movies.  \n",
    "These real ratings we collected inside a test matrix and substracted this information from train matrix. \n",
    "In each iteration through users and movies, we find top K movies that are the most similar to the given movie.\n",
    "Afterwards, we predict the rating user gave to movie.\n",
    "Lastly, we calculate MSE and MAE errors in order to see how eficiently we completed our task."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "ename": "MemoryError",
     "evalue": "",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mMemoryError\u001b[0m                               Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-11-fda7adb3a2e0>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m()\u001b[0m\n\u001b[1;32m     10\u001b[0m \u001b[1;31m#item_correlation[np.isnan(item_correlation)] = 0\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[1;32m     11\u001b[0m \u001b[1;31m#print(item_correlation.shape)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m---> 12\u001b[0;31m \u001b[0mitem_correlation\u001b[0m \u001b[1;33m=\u001b[0m \u001b[0mfast_user_similarity\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mtrain\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     13\u001b[0m \u001b[1;33m\u001b[0m\u001b[0m\n\u001b[1;32m     14\u001b[0m \u001b[1;32mfor\u001b[0m \u001b[0mk\u001b[0m \u001b[1;32min\u001b[0m \u001b[0mrange\u001b[0m\u001b[1;33m(\u001b[0m\u001b[1;36m2\u001b[0m\u001b[1;33m,\u001b[0m\u001b[1;36m11\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m:\u001b[0m \u001b[1;31m# from 2 because 1 cannot give rating to itself\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m<ipython-input-9-c3850575a709>\u001b[0m in \u001b[0;36mfast_user_similarity\u001b[0;34m(ratings, epsilon)\u001b[0m\n\u001b[1;32m      1\u001b[0m \u001b[1;32mdef\u001b[0m \u001b[0mfast_user_similarity\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mratings\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mepsilon\u001b[0m\u001b[1;33m=\u001b[0m\u001b[1;36m1e-9\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m:\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m----> 2\u001b[0;31m     \u001b[0msim\u001b[0m \u001b[1;33m=\u001b[0m \u001b[0mratings\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mdot\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mratings\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mT\u001b[0m\u001b[1;33m)\u001b[0m \u001b[1;33m+\u001b[0m \u001b[0mepsilon\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m      3\u001b[0m     \u001b[0mnorms\u001b[0m \u001b[1;33m=\u001b[0m \u001b[0mnp\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0marray\u001b[0m\u001b[1;33m(\u001b[0m\u001b[1;33m[\u001b[0m\u001b[0mnp\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0msqrt\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mnp\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mdiagonal\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0msim\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m]\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[1;32m      4\u001b[0m     \u001b[1;32mreturn\u001b[0m \u001b[1;33m(\u001b[0m \u001b[0msim\u001b[0m \u001b[1;33m/\u001b[0m \u001b[0mnorms\u001b[0m \u001b[1;33m/\u001b[0m \u001b[0mnorms\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mT\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n",
      "\u001b[0;31mMemoryError\u001b[0m: "
     ]
    }
   ],
   "source": [
    "predicted_rating_list = {}\n",
    "true_rating_list = {}\n",
    "error = {}\n",
    " \n",
    "train, test = train_test_split(ratings_matrix)   \n",
    "#print(train.shape)\n",
    "#print(train.T.shape)\n",
    "item_correlation = 1 - pairwise_distances(train, metric='correlation')\n",
    "item_correlation[np.isnan(item_correlation)] = 0\n",
    "\n",
    "for k in range(2,11): # from 2 because 1 cannot give rating to itself\n",
    "    predicted_rating_list = []\n",
    "    true_rating_list = []\n",
    "    for user_index in range(test.shape[0]):\n",
    "        if test[user_index, :].nonzero()[0].size:\n",
    "            for movie_index in test[user_index, :].nonzero()[0]:\n",
    "                #print(user_index)\n",
    "\n",
    "                similar_users = top_k_users(item_correlation, user_by_index, user_index, k)\n",
    "                similar_users_Indices = [user.Index for user in similar_users] \n",
    "                similar_users_Ratings = ratings_matrix[similar_users_Indices,movie_index]\n",
    "                \n",
    "                predicted_rating, true_rating = user_based_K_nearest_neighbours(user_index, movie_index, k)\n",
    "                #print(f\"predicted: {predicted_rating}, true: {true_rating}\")\n",
    "                predicted_rating_list.append(predicted_rating)\n",
    "                true_rating_list.append(true_rating)\n",
    "    \n",
    "    MSE, MAE = calculate_error(true_rating_list, predicted_rating_list)\n",
    "    error[k] = MSE, MAE\n",
    "    print(f'MSE = {MSE}, MAE = {MAE}')\n",
    "  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "Yes, we can see that we surely have a problem with memory. The size of user's similarity matrix is 72916 x 72916 which is much bigger than what could my 4GB RAM handle.  \n",
    "  \n",
    "While exploring on the Internet, I have found that the best solution to this problem would be not to use similarity matrix. Besides the fact that similarity matrices are usualy very sparse,  this case when it gets very big is one of the additional reasons why not to use similarity matrices in similar situations."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
